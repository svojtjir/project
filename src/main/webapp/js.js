$(document).ready(function()
{
  $('#pager').hide();
  $('#filter').hide();

  var current = {
    page: 0,
    resourceType: null,
    orderBy: null,
    orderDir: null,
    records: 0,
    destinations: null,
    fromDate: null,
    toDate: null,
    filterEnabled: false,
    admin: false
  };

  $('#prev').click(function()
  {
    current.page--;
    getPage(current.resourceType, current.page, current.orderBy, current.orderDir, current.fromDate, current.toDate);
  });

  $('#next').click(function()
  {
    current.page++;
    getPage(current.resourceType, current.page, current.orderBy, current.orderDir, current.fromDate, current.toDate);
  });

  var attributes = {
    destination: [
      {
        name: 'id',
        displayName: 'ID',
        type: 'int',
        editable: false,
        sortable: false
      },
      {
        name: 'name',
        displayName: 'Name',
        type: 'string',
        editable: true,
        sortable: true
      },
      {
        name: 'lat',
        displayName: 'Latitude',
        type: 'float',
        editable: false,
        sortable: false
      },
      {
        name: 'lon',
        displayName: 'Longitude',
        type: 'float',
        editable: false,
        sortable: false
      },
      {
        name: 'uri',
        displayName: 'URI',
        type: 'int',
        editable: false,
        sortable: false
      }
    ],
    flight: [
      {
        name: 'id',
        displayName: 'ID',
        type: 'int',
        editable: false,
        sortable: false
      },
      {
        name: 'name',
        displayName: 'Name',
        type: 'string',
        editable: true,
        sortable: true
      },
      {
        name: 'dateOfDeparture',
        displayName: 'Date of departure',
        type: 'date',
        editable: true,
        sortable: true
      },
      {
        name: 'distance',
        displayName: 'Distance',
        type: 'float',
        editable: false,
        sortable: true
      },
      {
        name: 'seats',
        displayName: 'Seats',
        type: 'int',
        editable: true,
        sortable: true
      },
      {
        name: 'price',
        displayName: 'Price',
        type: 'float',
        editable: false,
        sortable: true
      },
      {
        name: 'from',
        displayName: 'From',
        type: 'destination',
        editable: true,
        sortable: true
      },
      {
        name: 'to',
        displayName: 'To',
        type: 'destination',
        editable: true,
        sortable: true
      },
      {
        name: 'uri',
        displayName: 'URI',
        type: 'int',
        editable: false,
        sortable: false
      }
    ],
    reservation: [
      {
        name: 'id',
        displayName: 'ID',
        type: 'int',
        editable: false,
        sortable: false
      },
      {
        name: 'flight',
        displayName: 'Flight',
        type: 'int',
        editable: true,
        sortable: true
      },
      {
        name: 'created',
        displayName: 'Created',
        type: 'date',
        editable: true,
        sortable: true
      },
      {
        name: 'password',
        displayName: 'Password',
        type: 'string',
        editable: true,
        sortable: true
      },
      {
        name: 'seats',
        displayName: 'Seats',
        type: 'int',
        editable: true,
        sortable: true
      },
      {
        name: 'state',
        displayName: 'State',
        type: 'string',
        editable: true,
        sortable: true
      },
      {
        name: 'uri',
        displayName: 'URI',
        type: 'int',
        editable: false,
        sortable: false
      }
    ]
  };


  function getEntity(resourceType, id, password)
  {
    if(resourceType == 'destination' && current.destinations !== null)
    {
      for(var d = 0; d < current.destinations.length; d++)
        if(current.destinations[d].id == id)
          return current.destinations[d];
    }

    var headers = password ? {'X-Password': password} : undefined;

    var entity = null;
    printReq(8, 'GET', 'http://localhost:8080/aos/rest/'+resourceType+'/'+id, {}, headers);
    $.ajax({
      url: 'http://localhost:8080/aos/rest/'+resourceType+'/'+id,
      accepts: 'application/json',
      cache: false,
      contentType: 'application/json',
      type: 'GET',
      headers: headers,
      dataType: 'json',
      async: false,
      success: function(ent,status,xhr)
      {
        entity = ent;
      },
      error: function(data)
      {
        console.error('Request failed!', data);
        alert('Request failed!');
      }
    });
    return entity;
  }

  function buildPage(resourceType, pageNumber, orderBy, orderDir, entities)
  {
    $('#list').empty();

    var header = $('<tr>');
    $('#list').append(header);
    var attrs = attributes[resourceType];
    for(var a = 0; a < attrs.length; a++)
    {
      var cell = $('<th>');
      header.append(cell);
      cell.append(attrs[a].displayName);
      if(attrs[a].sortable)
      {
        var up = $('<button id="order-'+a+'">↑</button>');
        cell.append(up);
        var down = $('<button id="order-'+a+'">↓</button>');
        cell.append(down);

        up.click(function()
        {
          current.orderDir = 'DESC';
          var a = parseInt(this.id.substr(6));
          current.orderBy = attrs[a].name;
          getPage(resourceType, pageNumber, attrs[a].name, current.orderDir, current.fromDate, current.toDate);
        });

        down.click(function()
        {
          current.orderDir = 'ASC';
          var a = parseInt(this.id.substr(6));
          current.orderBy = attrs[a].name;
          getPage(resourceType, pageNumber, attrs[a].name, current.orderDir, current.fromDate, current.toDate);
        });
      }
    }
    header.append('<th>');

    for(var e = 0; e < entities[resourceType+'s'].length; e++)
    {
      var ent = entities[resourceType+'s'][e];
      var row = $('<tr id="record-'+e+'">');
      $('#list').append(row);
      for(var a = 0; a < attrs.length; a++)
      {
        if(attrs[a].type == 'destination')
        {
          var entity = getEntity('destination', ent[attrs[a].name]);
          var label = entity.name;
        }
        else
          label = ent[attrs[a].name];

        var cell = $('<td>'+label+'</td>');
        row.append(cell);
      }

      if(resourceType == 'flight')
      {
        var cell = $('<td>');
        row.append(cell);
        var res = $('<button id="reserve-'+ent.id+'">Reserve</button>');
        cell.append(res);
        //flight, seats
        res.click(function()
        {
          var id = parseInt(this.id.substr(8));

          var seats = prompt('How many seats do you wish to reserve?');
          if(seats === null)
            return;
          var data = '{"flight": '+id+', "seats": '+seats+'}';

          printReq(1, 'POST', 'http://localhost:8080/aos/rest/reservation', data, {});
          $.ajax({
            url: 'http://localhost:8080/aos/rest/reservation',
            accepts: 'application/json',
            cache: false,
            contentType: 'application/json',
            type: 'POST',
            dataType: 'json',
            async: false,
            data: data,
            success: function(res,status,xhr)
            {
              getPage('flight', pageNumber, orderBy, orderDir, current.fromDate, current.toDate);
              alert('Thank you for reserving the flight.\nReservation password: '+res.password);
            },
            error: function(data)
            {
              console.error('Request failed!', data);
              alert('Request failed!');
            }
          });
        });
      }

      if(!current.admin)
        continue;

      //Edit

      var cell = $('<td>');
      row.append(cell);
      var edit = $('<button id="edit-'+e+'">Edit</button>');
      cell.append(edit);
      edit.click(function()
      {
        var id = parseInt(this.id.substr(5));
        var cells = $('#record-'+id).children();

        if(this.innerHTML == 'Edit')
        {
          for(var a = 0; a < attrs.length; a++)
            if(attrs[a].editable)
            {
              if(attrs[a].type == 'destination')
              {
                cells.eq(a).empty();
                var select = $('<select>');
                cells.eq(a).append(select);

                for(var d = 0; d < current.destinations.length; d++)
                  select.append('<option value="'+current.destinations[d].id+'">'+current.destinations[d].name+'</option>');
              }
              else
              {
                var input = $('<input type="text" value="'+cells.eq(a).html()+'"/>');
                cells.eq(a).html(input);
                if(attrs[a].type == 'date')
                  input.addClass('date');
              }
            }

          this.innerHTML = 'Save';
        }
        else
        {
          var entity = {};
          for(var a = 0; a < attrs.length; a++)
          {
            if(attrs[a].editable)
            {
              var value = cells.eq(a).children().val();
              cells.eq(a).html(value);
              entity[attrs[a].name] = value;
            }
          }

          var data = serializeEntity(resourceType, entity);

          printReq(2, 'PUT', 'http://localhost:8080/aos/rest/'+resourceType+'/'+ent.id, data, {});
          $.ajax({
            url: 'http://localhost:8080/aos/rest/'+resourceType+'/'+ent.id,
            accepts: 'application/json',
            cache: false,
            contentType: 'application/json',
            type: 'PUT',
            dataType: 'json',
            data: data,
            async: false,
            success: function(dest,status,xhr)
            {
              getPage(resourceType, pageNumber, orderBy, orderDir, current.fromDate, current.toDate);
              if(resourceType == 'destination')
                current.destinations = getAllEntities('destination');
            },
            error: function(data)
            {
              console.error('Request failed!', data);
              alert('Request failed!');
            }
          });

          this.innerHTML = 'Edit';
        }
      });

      //Remove

      var cell = $('<td>');
      row.append(cell);
      var del = $('<button id="delete-'+e+'">X</button>');
      cell.append(del);
      del.click(function()
      {
        var id = parseInt(this.id.substr(7));

        printReq(3, 'DELETE', 'http://localhost:8080/aos/rest/'+resourceType+'/'+ent.id, {}, {});
        $.ajax({
          url: 'http://localhost:8080/aos/rest/'+resourceType+'/'+ent.id,
          accepts: 'application/json',
          cache: false,
          contentType: 'application/json',
          type: 'DELETE',
          dataType: 'json',
          async: false,
          success: function(dest,status,xhr)
          {
            getPage(resourceType, pageNumber, orderBy, orderDir, current.fromDate, current.toDate);
            if(resourceType == 'destination')
              current.destinations = getAllEntities('destination');
          },
          error: function(data)
          {
            console.error('Request failed!', data);
            alert('Request failed!');
          }
        });
      });
    }

    if(!current.admin)
      return;

    var row = $('<tr>');
    $('#list').append(row);
    for(var a = 0; a < attrs.length; a++)
    {
      var cell = $('<td>');
      row.append(cell);
      if(attrs[a].editable)
      {
        if(attrs[a].type == 'destination')
        {
          var select = $('<select>');
          cell.append(select);

          var destinations = current.destinations.destinations;

          for(var d = 0; d < destinations.length; d++)
            select.append('<option value="'+destinations[d].id+'">'+destinations[d].name+'</option>');
        }
        else
        {
          var input = $('<input type="text"/>');
          cell.html(input);
          if(attrs[a].type == 'date')
            input.addClass('date');
        }
      }
    }
    var cell = $('<td>');
    var addButton = $('<button>Add</button>');
    cell.append(addButton);
    row.append(cell);

    addButton.click(function()
    {
      var entity = {};
      for(var a = 0; a < attrs.length; a++)
        entity[attrs[a].name] = row.children().eq(a).children().val();

      var data = serializeEntity(resourceType, entity);

      printReq(4, 'POST', 'http://localhost:8080/aos/rest/'+resourceType, data, {});
      $.ajax({
        url: 'http://localhost:8080/aos/rest/'+resourceType,
        accepts: 'application/json',
        cache: false,
        contentType: 'application/json',
        type: 'POST',
        dataType: 'json',
        data: data,
        async: false,
        success: function(dest,status,xhr)
        {
          getPage(resourceType, pageNumber, orderBy, orderDir, current.fromDate, current.toDate);
          if(resourceType == 'destination')
            current.destinations = getAllEntities('destination');
        },
        error: function(dat)
        {
          console.error('Request failed!', data);
          alert('Request failed!');
        }
      });
    });
  }

  function serializeEntity(resourceType, entity)
  {
    var attrs = attributes[resourceType];
    var data = '{';
    for(var a = 0; a < attrs.length; a++)
    {
      if(attrs[a].editable)
      {
        switch(attrs[a].type)
        {
          case 'string':
          case 'date':
            data += '"'+attrs[a].name+'": "'+entity[attrs[a].name]+'",';
            break;
          case 'float':
          case 'int':
            data += '"'+attrs[a].name+'": '+entity[attrs[a].name]+',';
            break;
          case 'destination':
            data += '"'+attrs[a].name+'": '+entity[attrs[a].name]+',';
            break;
          default:
            console.error('unrecognized type: '+attrs[a].type);
        }
      }
    }
    data = data.substr(0, data.length-1) + '}';

    return data;
  }

  function getPage(resourceType, pageNumber, orderBy, orderDir, fromDate, toDate)
  {
    $('#pager').show();
    $('#reservation').hide();

    var headers = {
      'X-Base': 10,
      'X-Offset': pageNumber * 10
    };

    if(orderBy)
      headers['X-Order'] = orderBy+':'+orderDir;

    if(fromDate && toDate && current.filterEnabled)
    {
       headers['X-Filter'] = 'dateOfDepartureFrom='+fromDate+',dateOfDepartureTo='+toDate;
    }

//     headers['X-Currency'] = 'EUR';

    printReq(5, 'GET', 'http://localhost:8080/aos/rest/'+resourceType, {}, headers);
    $.ajax({
      url: 'http://localhost:8080/aos/rest/'+resourceType,
      accepts: 'application/json',
      cache: false,
      contentType: 'application/json',
      type: 'GET',
      dataType: 'json',
      headers: headers,
      async: false,
      success: function(entities,status,xhr)
      {
        var index = xhr.getAllResponseHeaders().indexOf('X-Count-records:')+16;
        var records = xhr.getAllResponseHeaders().substr(index);
        var i = 0;
        while(records[i] != '\n')
          i++;
        records = parseInt(records.substr(0, i));

        $('#prev').prop('disabled', pageNumber == 0);
        $('#next').prop('disabled', records <= (pageNumber+1)*10);

        var pagesTotal = Math.ceil(records/10);
        if(pagesTotal == 0)
          pagesTotal = 1;

        if(pageNumber == pagesTotal)
        {
          getPage(resourceType, pageNumber-1, orderBy, orderDir, current.fromDate, current.toDate);
        }
        else
        {
          $('#page').html('Page '+(pageNumber+1)+' of '+pagesTotal);
          current.records = records;
          buildPage(resourceType, pageNumber, orderBy, orderDir, entities);
        }
      },
      error: function(data)
      {
        console.error('Request failed!', data);
        alert('Request failed!');
      }
    });
  }

  $('#destinations').click(function()
  {
    current.resourceType = 'destination';
    getPage('destination', 0);
    $('#filter').hide();
  });

  $('#flights').click(function()
  {
    current.resourceType = 'flight';
    if(current.destinations == null)
      current.destinations = getAllEntities('destination');
    getPage('flight', 0);
    $('#filter').show();
    $('#filterInputs').hide();
    $('#filterActive').trigger('change');
  });

  $('#reservations').click(function()
  {
    current.resourceType = 'reservation';
    getPage('reservation', 0);
    $('#filter').hide();
  });

  $('#filterActive').change(function(a,b,c)
  {
    if($('#filterActive').prop('checked'))
    {
      $('#filterInputs').show();
    }
    else
    {
      $('#filterInputs').hide();
      current.filterEnabled = false;
      getPage(current.resourceType, current.page, current.orderBy, current.orderDir);
    }
  });

  $('#filterApply').click(function()
  {
    current.fromDate = $('#fromDate').val();
    current.toDate = $('#toDate').val();
    current.filterEnabled = true;
    getPage(current.resourceType, current.page, current.orderBy, current.orderDir, current.fromDate, current.toDate);
  });

  $('#reservations').hide();

  $('#workAsAdmin-box').change(function()
  {
    if((current.admin = $(this).prop('checked')))
    {
      $('#reservations').show();
    }
    else
    {
      $('#reservations').hide();
    }
  });

  examples = {
    destination: [
      {
        name: 'Brno'
      },
      {
        name: 'Praha'
      },
      {
        name: 'Lubnik'
      },
      {
        name: 'Kochov'
      }
    ],
    flight: [
      {
        name: 'Z Prahy do Brna',
        dateOfDeparture: '2014-10-21T17:24:06+02:00',
        seats: 214,
        from: 2,
        to: 1
      },
      {
        name: 'Z Brna do Prahy',
        dateOfDeparture: '2014-10-22T17:24:06+02:00',
        seats: 56,
        from: 1,
        to: 2
      },
      {
        name: 'Z Lubnika do Brna',
        dateOfDeparture: '2014-11-21T17:24:06+02:00',
        seats: 356,
        from: 3,
        to: 1
      },
      {
        name: 'Z Kochova do Lubnika',
        dateOfDeparture: '2014-10-26T17:24:06+02:00',
        seats: 68,
        from: 4,
        to: 3
      },
      {
        name: 'Z Prahy do Kochova',
        dateOfDeparture: '2014-10-21T10:24:06+02:00',
        seats: 136,
        from: 2,
        to: 4
      },
    ]
  };

  $('#examples').click(function()
  {
    for(var resourceType in examples)
    {
      for(var i = 0; i < examples[resourceType].length; i++)
      {
        var data = serializeEntity(resourceType, examples[resourceType][i]);

        printReq(6, 'POST', 'http://localhost:8080/aos/rest/'+resourceType, data, {});
        $.ajax({
          url: 'http://localhost:8080/aos/rest/'+resourceType,
          accepts: 'application/json',
          cache: false,
          contentType: 'application/json',
          type: 'POST',
          dataType: 'json',
          data: data,
          async: false,
          error: function(dat)
          {
            console.error('Request failed!', data);
          }
        });
      }
    }
    $(this).remove();
  });

  function getAllEntities(resourceType)
  {
    var entities = null;
    printReq(7, 'GET', 'http://localhost:8080/aos/rest/'+resourceType, {}, {});
    $.ajax({
      url: 'http://localhost:8080/aos/rest/'+resourceType,
      accepts: 'application/json',
      cache: false,
      contentType: 'application/json',
      type: 'GET',
      dataType: 'json',
      async: false,
      success: function(ent,status,xhr)
      {
        entities = ent;
      },
      error: function(data)
      {
        console.error('Request failed!', data);
        alert('Request failed!');
      }
    });
    return entities;
  }

  function printReq(id, method, uri, data, headers)
  {
    console.error('['+id+']', method, uri, data, headers);
  }

  $('#reservation').hide();
  $('#showReservation').click(function()
  {
    var res = getEntity('reservation', $('#id').val(), $('#password').val());
    $('#reservationId').html(res.id);
    $('#reservationFlight').html(getEntity('flight', res.flight).name);
    $('#reservationCreated').html(res.created);
    $('#reservationPassword').html(res.password);
    $('#reservationSeats').html(res.seats);
    $('#reservationState').html(res.state);
    $('#reservationUri').html(res.uri);
    $('#reservation').show();
    $('#printReservation').prop('disabled', (res.state != 'paid'));
    $('#cancelReservation').prop('disabled', (res.state != 'new'));
  });

  $('#printReservation').click(function()
  {
    var headers = {'X-Password': $('#reservationPassword').html()};
    var id = $('#reservationId').html();
    printReq(9, 'GET', 'http://localhost:8080/aos/rest/reservation/'+id+'/print', {}, headers);
    $.ajax({
      url: 'http://localhost:8080/aos/rest/reservation/'+id+'/print',
      accepts: 'application/octect-stream',
      cache: false,
      type: 'GET',
      headers: headers,
      async: false,
      success: function(ticket)
      {
        alert(ticket);
      },
      error: function(dat)
      {
        console.error('Request failed!', data);
      }
    });
  });

  $('#cancelReservation').click(function()
  {
    var data = '{"state": "canceled"}';
    var id = $('#reservationId').html();
    printReq(10, 'PUT', 'http://localhost:8080/aos/rest/reservation/'+id, data, {});
    $.ajax({
      url: 'http://localhost:8080/aos/rest/reservation/'+id,
      accepts: 'application/json',
      cache: false,
      contentType: 'application/json',
      type: 'PUT',
      headers: {'X-Password': $('#reservationPassword').html()},
      dataType: 'json',
      data: data,
      async: false,
      success: function()
      {
        $('#reservationState').html('canceled');
        $('#cancelReservation').prop('disabled', true);
      },
      error: function(dat)
      {
        console.error('Request failed!', data);
      }
    });
  });

  var websocket = new WebSocket('ws://localhost:8080/aos/connected');

  websocket.onopen = function (evt) {
    console.log('onOpen', evt);
  };

  websocket.onmessage = function (evt)
  {
    $('#connected-clients').html(evt.data);
  };

  websocket.onerror = function (evt) {
    console.log('error', evt);
  };
});
