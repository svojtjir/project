/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.services;

import cz.cvut.fel.aos.email.EmailMessage;
import cz.cvut.fel.aos.entities.Reservation;
import java.net.MalformedURLException;
import java.net.URL;
import javax.jms.Queue;
import javax.annotation.Resource;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;

/**
 * This class implements a print service.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
@WebService(serviceName = "PrintService")
//public class PrintService extends Service
public class PrintService
{
    static private URL wsdl;
    static private QName name;
    
    @Resource(mappedName = "java:/ConnectionFactory")
    private ConnectionFactory connectionFactory;

    @Resource(mappedName = "java:/queue/EmailQueue")
    private Queue queue;

    static
    {
        try
        {
            PrintService.wsdl = new URL("http://localhost:8080/aos/PrintService?wsdl");
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        PrintService.name = new QName("http://printService.aos.fel.cvut.cz/", "PrintService");
    }
    

    
//    public PrintService(URL wsdlLocation, QName serviceName)
//    {
//        super(wsdlLocation, serviceName);
//    }

//    public PrintService()
//    {
//        super(PrintService.wsdl, PrintService.name);
//    }

    /**
     * Sends a JMS message to the email sender which performs the email sending. The email contains information about paid flight reservation.
     *
     * @param reservation The reservation to inform about
     * @param email An email address to send the information to
     */
    @WebMethod(operationName = "sendEmail")
    public void sendEmail(@WebParam(name = "reservation") Reservation reservation, @WebParam(name = "email") String email)
    {
        try
        {
            Connection connection = this.connectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer((javax.jms.Destination) queue);
            messageProducer.setDeliveryMode(DeliveryMode.PERSISTENT);
            messageProducer.setTimeToLive(999999);

            EmailMessage emailMessage = new EmailMessage(email, reservation);
            ObjectMessage message = session.createObjectMessage();
            message.setObject(emailMessage);
            messageProducer.send(message);
            connection.close();
        }
        catch (JMSException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Returns a port of the service.
     *
     * @return The port
     */
    @WebEndpoint(name = "PrintServicePort")
    public PrintService getPrintServicePort() {
        QName PrintServicePort = new QName("http://printService.aos.fel.cvut.cz/", "PrintServicePort");
        return this;
//        Service s = new Service(Print.wsdl, Print.name);
//        return super.getPort(PrintServicePort, Print.class);
    }
}
