/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.services;

import cz.cvut.fel.aos.dbentities.FlightEntity;
import cz.cvut.fel.aos.entities.Reservation;
import cz.cvut.fel.aos.dbentities.ReservationEntity;
import cz.cvut.fel.aos.entities.Payment;
import cz.cvut.fel.aos.exceptions.BadRequestException;
import cz.cvut.fel.aos.exceptions.ForbiddenException;
import cz.cvut.fel.aos.persistence.FlightDao;
import cz.cvut.fel.aos.persistence.ReservationDao;
import cz.cvut.fel.aos.utils.Utils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * This class implements a service providing flight reservations.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
public class ReservationService
{
    private final ReservationDao reservationDao = new ReservationDao();
    private final FlightDao flightDao = new FlightDao();

    /**
     * Returns a single reservation.
     *
     * @param id ID of the reservation
     * @param password Password to access the reservation
     * @return The requested reservation
     */
    public Reservation get(Long id, String password)
    {
        ReservationEntity entity = reservationDao.get(id);
        if(!entity.getPassword().equals(password))
            throw new ForbiddenException("Wrong password");
        Reservation res = entityToReservation(entity);
        res.setUri("/reservation/"+id.toString());
        return res;
    }

    /**
     * Returns multiple reservations.
     *
     * @param orderBy  Property to be used as an ordering criterion; can be null
     * @param orderDir Ordering direction; can be null or either ASC or DESC
     * @param base     Maximal number of entities to return; use 0 for no limit
     * @param offset   Offset of the entities to return
     * @return The requested reservations
     */
    public List<Reservation> get(String orderBy, String orderDir, int base, int offset)
    {
        List<Reservation> reservations = new ArrayList<Reservation>();
        List<ReservationEntity> entities = reservationDao.get(orderBy, orderDir, base, offset);
        for(ReservationEntity entity : entities)
        {
            Reservation res = entityToReservation(entity);
            res.setUri("/reservation/"+res.getId().toString());
            reservations.add(res);
        }
        return reservations;
    }

    /**
     * Returns the count of all reservations.
     *
     * @return The count
     */
    public Long getCount()
    {
        return reservationDao.getCount();
    }
    
    /**
     * Removes a reservation.
     *
     * @param id ID of the reservation to remove
     */
    public Reservation delete(Long id)
    {
        Reservation res = entityToReservation(reservationDao.get(id));
        if(res.getState().equals("paid"))
            throw new BadRequestException("Cannot delete a paid reservation");
        reservationDao.delete(id);
        res.setUri("/reservation/"+id.toString());
        return res;
    }

    /**
     * Creates a new reservation.
     *
     * @param res The reservation to create
     */
    public Reservation add(Reservation res)
    {
        res.setCreated(Utils.dateLongToString((new Date()).getTime()));
        res.setPassword(UUID.randomUUID().toString());
        res.setState("new");
        
        FlightEntity flight = flightDao.get(res.getFlight());
        int seatsAvailable = flight.getSeats();
        int seatsRequested = res.getSeats();
        
        //check whether there's enough available seats
        if(seatsRequested > seatsAvailable)
            throw new BadRequestException("Cannot satisfy the reservation request: "+String.valueOf(seatsAvailable)+" seats available, "+String.valueOf(seatsRequested)+" requested");
        
        //update available seats
        flight.setSeats(seatsAvailable-seatsRequested);
        flightDao.update(flight);

        //create reservation record
        ReservationEntity entity = reservationToEntity(res);
        reservationDao.add(entity);
        res.setId(entity.getId());
        res.setUri("/reservation/"+res.getId().toString());
        return res;
    }
    
    /**
     * Updates a reservation.
     *
     * @param id ID of the reservation to update
     * @param res The reservation to update
     * @param password Password to access the reservation
     */
    public Reservation update(Long id, Reservation res, String password)
    {
        res.setId(id);
        ReservationEntity entity = reservationDao.get(res.getId());
        
        //password validation
        if(!entity.getPassword().equals(password))
            throw new ForbiddenException("Wrong password");

        String prevState = entity.getState();
        String newState = res.getState();
        
        if(!prevState.equals("canceled") && newState.equals("canceled"))
        {
            FlightEntity flight = flightDao.get(entity.getFlight());
            flight.setSeats(flight.getSeats() + entity.getSeats());
            flightDao.update(flight);
        }

        //update properties
        if(res.getFlight() != null)
            entity.setFlight(res.getFlight());
        if(res.getCreated() != null)
            entity.setCreated(res.getCreated());
        if(res.getPassword() != null)
            entity.setPassword(res.getPassword());
        if(res.getSeats() != 0)
            entity.setSeats(res.getSeats());
        if(res.getState() != null)
            entity.setState(res.getState());

        reservationDao.update(entity);
        res = entityToReservation(entity);
        res.setUri("/reservation/"+id.toString());
        return res;
    }
    
    /**
     * Creates a payment.
     *
     * @param reservationId ID of the reservation to pay
     * @param payment The payment to create
     */
    public Payment addPayment(Long reservationId, Payment payment)
    {
        ReservationEntity entity = reservationDao.get(reservationId);
        if(!entity.getState().equals("new"))
            throw new BadRequestException("Cannot accept payment of a "+entity.getState()+" reservation.");
        entity.setState("paid");
        reservationDao.update(entity);

        return payment;
    }

    // =========== Helpers ================

    private Reservation entityToReservation(ReservationEntity entity)
    {
        Reservation reservation = new Reservation();
        if (entity != null)
        {
            reservation.setId(entity.getId());
            reservation.setFlight(entity.getFlight());
            reservation.setCreated(Utils.dateLongToString(entity.getCreated()));
            reservation.setPassword(entity.getPassword());
            reservation.setSeats(entity.getSeats());
            reservation.setState(entity.getState());
        }
        return reservation;
    }

    private ReservationEntity reservationToEntity(Reservation reservation)
    {
        ReservationEntity entity = new ReservationEntity();
        if (reservation != null)
        {
            entity.setId(reservation.getId());
            entity.setFlight(reservation.getFlight());
            entity.setCreated(Utils.dateStringToLong(reservation.getCreated()));
            entity.setPassword(reservation.getPassword());
            entity.setSeats(reservation.getSeats());
            entity.setState(reservation.getState());
        }
        return entity;
    }
}
