/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.services;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import cz.cvut.fel.aos.entities.Destination;
import cz.cvut.fel.aos.dbentities.DestinationEntity;
import cz.cvut.fel.aos.persistence.DestinationDao;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ws.rs.core.UriBuilder;

/**
 * This class implements a service providing flight destinations.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
public class DestinationService
{
    private final DestinationDao destinationDao = new DestinationDao();

    /**
     * Returns a single destination.
     *
     * @param id ID of the destination
     * @return The requested destination
     */
    public Destination get(Long id)
    {
        DestinationEntity entity = destinationDao.get(id);
        Destination dest = entityToDestination(entity);
        dest.setUri("/destination/"+id.toString());
        return dest;
    }
    
    /**
     * Returns multiple destinations.
     *
     * @param orderBy  Property to be used as an ordering criterion; can be null
     * @param orderDir Ordering direction; can be null or either ASC or DESC
     * @param base     Maximal number of entities to return; use 0 for no limit
     * @param offset   Offset of the entities to return
     * @return The requested destinations
     */
    public List<Destination> get(String orderBy, String orderDir, int base, int offset)
    {
        List<Destination> destinations = new ArrayList<Destination>();
        UriBuilder uri = null;
        List<DestinationEntity> entities = destinationDao.get(orderBy, orderDir, base, offset);
        for(DestinationEntity entity : entities)
        {
            Destination dest = entityToDestination(entity);
            dest.setUri("/destination/"+dest.getId().toString());
            destinations.add(dest);
        }
        return destinations;
    }
    
    /**
     * Returns the count of all destinations.
     *
     * @return The count
     */
    public Long getCount()
    {
        return destinationDao.getCount();
    }  

    /**
     * Removes a destination.
     *
     * @param id ID of the destination to remove
     */
    public Destination delete(Long id)
    {
        Destination dest = entityToDestination(destinationDao.get(id));
        destinationDao.delete(id);
        dest.setUri("/destination/"+id.toString());
        return dest;
    }

    /**
     * Creates a new destination.
     *
     * @param destination The destination to create
     */
    public Destination add(Destination dest)
    {
        //get coordinates of the destination
        Client client = Client.create();
        WebResource webResource = client.resource("http://maps.googleapis.com/maps/api/geocode/json?address="+dest.getName()+"&sensor=false");
        ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
        if(response.getStatus() == 200)
        {
            String json = response.getEntity(String.class);
            //parsing
            Pattern pattern = Pattern.compile("location\"\\s*:\\s*\\{\\s*\"lat\"\\s*:\\s*([0-9.]+)\\s*,\\s*\"lng\"\\s*:\\s*([0-9.]+)");
            Matcher matcher = pattern.matcher(json);
            matcher.find();
            dest.setLat(Float.valueOf(matcher.group(1)));
            dest.setLon(Float.valueOf(matcher.group(2)));
        }
        else
          throw new RuntimeException("Failed : HTTP error code : "+ response.getStatus());
        
        DestinationEntity entity = destinationToEntity(dest);
        destinationDao.add(entity);
        dest.setId(entity.getId());
        dest.setUri("/destination/"+dest.getId().toString());
        return dest;
    }
    
    /**
     * Updates a destination.
     *
     * @param destination The destination to update
     */
    public Destination update(Long id, Destination dest)
    {
        dest.setId(id);
        DestinationEntity entity = destinationToEntity(dest);
        destinationDao.get(dest.getId());
        destinationDao.update(entity);
        dest = entityToDestination(entity);
        dest.setUri("/destination/"+id.toString());
        return dest;
    }

    private Destination entityToDestination(DestinationEntity entity)
    {
        Destination destination = new Destination();
        if (entity != null)
        {
            destination.setId(entity.getId());
            destination.setName(entity.getName());
            destination.setLat(entity.getLat());
            destination.setLon(entity.getLon());
        }
        return destination;
    }

    private DestinationEntity destinationToEntity(Destination destination)
    {
        DestinationEntity entity = new DestinationEntity();
        if (destination != null)
        {
            entity.setId(destination.getId());
            entity.setName(destination.getName());
            entity.setLat(destination.getLat());
            entity.setLon(destination.getLon());
        }
        return entity;
    }
}
