/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.services;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import cz.cvut.fel.aos.entities.Flight;
import cz.cvut.fel.aos.dbentities.FlightEntity;
import cz.cvut.fel.aos.entities.Destination;
import cz.cvut.fel.aos.persistence.FlightDao;
import cz.cvut.fel.aos.utils.Utils;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class implements a service providing flights.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
public class FlightService
{
    private final FlightDao flightDao = new FlightDao();
    private final DestinationService destinationService = new DestinationService();

    /**
     * Returns a single flight.
     *
     * @param id ID of the flight
     * @return The requested flight
     */
    public Flight get(Long id)
    {
        FlightEntity entity = flightDao.get(id);
        Flight flight = entityToFlight(entity);
        flight.setUri("/flight/"+id.toString());
        return flight;
    }
    
    /**
     * Returns multiple flights.
     *
     * @param departureFrom Lowest daparture date of flights to be included in the result
     * @param departureTo Highest daparture date of flights to be included in the result
     * @param orderBy  Property to be used as an ordering criterion; can be null
     * @param orderDir Ordering direction; can be null or either ASC or DESC
     * @param base     Maximal number of entities to return; use 0 for no limit
     * @param offset   Offset of the entities to return
     * @param currency The currency to apply on returning flight prices
     * @return The requested flights
     */
    public List<Flight> get(String departureFrom, String departureTo, String orderBy, String orderDir, int base, int offset, String currency)
    {
        List<Flight> flights = new ArrayList<Flight>();
        List<FlightEntity> entities = flightDao.get(departureFrom, departureTo, orderBy, orderDir, base, offset);
        for(FlightEntity entity : entities)
        {
            Flight flight = entityToFlight(entity);
            flight.setUri("/flight/"+flight.getId().toString());
            float rate = this.getCurrencyRate(currency);
            flight.setPrice(Math.round(flight.getPrice() * rate));
            flights.add(flight);
        }
        return flights;
    }
    
    /**
     * Returns the count of all flights stored in the database.
     *
     * @return The count
     */
    public Long getCount(String departureFrom, String departureTo)
    {
        return flightDao.getCount(departureFrom, departureTo);
    }

    /**
     * Removes a flight from the database.
     *
     * @param id ID of the flight to remove
     */
    public Flight delete(Long id)
    {
        Flight flight = entityToFlight(flightDao.get(id));
        flightDao.delete(id);
        flight.setUri("/flight/"+id.toString());
        return flight;
    }

    /**
     * Inserts a new flight into the database.
     *
     * @param flight The flight to insert
     */
    public Flight add(Flight flight)
    {
        //get flight distance and price
        Destination from = destinationService.get(flight.getFrom());
        Destination to = destinationService.get(flight.getTo());
        Client client = Client.create();
        WebResource webResource = client.resource("https://orfeomorello-flight.p.mashape.com/mashape/distancebetweenpoints/startlat/"+from.getLat()+"/startlng/"+from.getLon()+"/endlat/"+to.getLat()+"/endlng/"+to.getLon()+"/unit/K");
        ClientResponse response = webResource.header("X-Mashape-Key", "jIWrAHeAfzmshtDgrC0cU8t5s9ldp10iYA1jsnAzIojOA1pZPF").accept("application/json").get(ClientResponse.class);
        if(response.getStatus() == 200)
        {
            String json = response.getEntity(String.class);
            //parsing
            Pattern pattern = Pattern.compile("([0-9]+)");
            Matcher matcher = pattern.matcher(json);
            matcher.find();
            int distance = Integer.valueOf(matcher.group(1));
            flight.setDistance(distance);
            flight.setPrice(distance * 10);
        }
        else
          throw new RuntimeException("Failed : HTTP error code : "+ response.getStatus());

        FlightEntity entity = flightToEntity(flight);
        flightDao.add(entity);
        flight.setId(entity.getId());
        flight.setUri("/flight/"+flight.getId().toString());
        return flight;
    }
    
    /**
     * Updates a flight.
     *
     * @param flight The flight to update
     */
    public Flight update(Long id, Flight flight)
    {
        flight.setId(id);
        FlightEntity entity = flightToEntity(flight);
        flightDao.get(flight.getId());
        flightDao.update(entity);
        flight = entityToFlight(entity);
        flight.setUri("/flight/"+id.toString());
        return flight;
    }

    // =========== Helpers ================

    private Flight entityToFlight(FlightEntity entity)
    {
        Flight flight = new Flight();
        if (entity != null)
        {
            flight.setId(entity.getId());
            flight.setName(entity.getName());
            flight.setDateOfDeparture(Utils.dateLongToString(entity.getDateOfDeparture()));
            flight.setDistance(entity.getDistance());
            flight.setSeats(entity.getSeats());
            flight.setPrice(entity.getPrice());
            flight.setFrom(entity.getFrom());
            flight.setTo(entity.getTo());
        }
        return flight;
    }

    private FlightEntity flightToEntity(Flight flight)
    {
        FlightEntity entity = new FlightEntity();
        if (flight != null)
        {
            entity.setId(flight.getId());
            entity.setName(flight.getName());
            entity.setDateOfDeparture(Utils.dateStringToLong(flight.getDateOfDeparture()));
            entity.setDistance(flight.getDistance());
            entity.setSeats(flight.getSeats());
            entity.setPrice(flight.getPrice());
            entity.setFrom(flight.getFrom());
            entity.setTo(flight.getTo());
        }
        return entity;
    }
    
    private float getCurrencyRate(String currency)
    {
        if(currency == null)
            return (float) 1;
        Client client = Client.create();
        WebResource webResource = client.resource("http://rate-exchange.appspot.com/currency?from=CZK&to="+currency);
        ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
        if(response.getStatus() == 200)
        {
            String json = response.getEntity(String.class);
            System.out.println("json:"+json);
            //parsing
            Pattern pattern = Pattern.compile("([0-9]+)");
            Matcher matcher = pattern.matcher(json);
            matcher.find();
            return Float.valueOf(matcher.group(1));
        }
        else
          throw new RuntimeException("Failed : HTTP error code : "+ response.getStatus());
    }
}
