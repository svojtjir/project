/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.entities.mapping;

import cz.cvut.fel.aos.entities.Reservation;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class maps a set of reservations into XML structure.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
@XmlRootElement(name="aos-reservations")
public class ReservationsMapping
{
    private List<Reservation> reservations;
    
    /**
     * Default constructor
     */
    public ReservationsMapping(){}
    
    /**
     * Returns a list of assigned reservations.
     *
     * @return The reservations
     */
    public List<Reservation> getReservations()
    {
        return this.reservations;
    }
    
    /**
     * Assigns a list of reservations to the mapping object.
     *
     * @param reservations A list to assign
     */
    public void setReservations(List<Reservation> reservations)
    {
        this.reservations = reservations;
    }
}
