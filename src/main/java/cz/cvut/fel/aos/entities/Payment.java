/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.entities;

import java.io.Serializable;

/**
 * This class represents a flight reservation payment entity.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
public class Payment implements Serializable
{
    private Long accountNumber;

    /**
     * Returns account number of the payment.
     *
     * @return The account number
     */
    public Long getAccountNumber()
    {
        return accountNumber;
    }

    /**
     * Sets account number of the payment.
     *
     * @param number The account number to set
     */
    public void setAccountNumber(Long number)
    {
        this.accountNumber = number;
    }

    /**
     * Implements the hashCode method for this class.
     *
     * @return The hash code
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((accountNumber == null) ? 0 : accountNumber.hashCode());
        return result;
    }
}
