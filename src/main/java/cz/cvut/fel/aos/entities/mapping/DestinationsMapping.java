/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.entities.mapping;

import cz.cvut.fel.aos.entities.Destination;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class maps a set of destinations into XML structure.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
@XmlRootElement(name="aos-destinations")
public class DestinationsMapping
{
    private List<Destination> destinations;
    
    /**
     * Default constructor
     */
    public DestinationsMapping(){}
    
    /**
     * Returns a list of assigned destinations.
     *
     * @return The destinations
     */
    public List<Destination> getDestinations()
    {
        return this.destinations;
    }
    
    /**
     * Assigns a list of destinations to the mapping object.
     *
     * @param destinations A list to assign
     */
    public void setDestinations(List<Destination> destinations)
    {
        this.destinations = destinations;
    }
}
