/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.entities;

import cz.cvut.fel.aos.dbentities.FlightEntity;
import java.io.Serializable;

/**
 * This class represents a flight entity.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
public class Flight implements Serializable
{
    private Long id;
    private String name;
    private String dateOfDeparture;
    private int distance;
    private int seats;
    private int price;
    private Long from;
    private Long to;
    private String uri;

    /**
     * Returns ID of the flight entity.
     *
     * @return ID of the entity.
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Sets ID of the flight entity.
     *
     * @param id ID of the entity
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Returns name of the flight entity.
     *
     * @return Name of the entity.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets name of the flight entity.
     *
     * @param name Name of the entity
     */
    public void setName(String name)
    {
        this.name = name;
    }
    
    /**
     * Returns date of departure of the flight entity.
     *
     * @return Date of departure of the entity.
     */
    public String getDateOfDeparture()
    {
        return dateOfDeparture;
    }

    /**
     * Sets date of departure of the flight entity.
     *
     * @param dateOfDeparture Date of departure of the entity as a string in format ISO8601.
     */
    public void setDateOfDeparture(String dateOfDeparture)
    {
        this.dateOfDeparture = dateOfDeparture;
    }
    
    /**
     * Returns distance of the flight.
     *
     * @return Distance of the flight.
     */
    public int getDistance()
    {
        return distance;
    }

    /**
     * Sets distance of the flight.
     *
     * @param distance Distance of the flight
     */
    public void setDistance(int distance)
    {
        this.distance = distance;
    }
    
    /**
     * Returns number of seats in the plane.
     *
     * @return Number of seats in the plane
     */
    public int getSeats()
    {
        return seats;
    }

    /**
     * Sets number of seats in the plane.
     *
     * @param seats Number of seats in the plane
     */
    public void setSeats(int seats)
    {
        this.seats = seats;
    }
    
    /**
     * Returns price of a single ticket.
     *
     * @return Price of a single ticket
     */
    public int getPrice()
    {
        return price;
    }
    
    /**
     * Sets price of a single ticket.
     *
     * @param price Price of a single ticket
     */
    public void setPrice(int price)
    {
        this.price = price;
    }
    
    /**
     * Returns ID of the start destination of the flight.
     *
     * @return ID of the start destination of the flight
     */
    public Long getFrom()
    {
        return from;
    }

    /**
     * Sets ID of the start destination of the flight.
     *
     * @param from ID of the start destination of the flight
     */
    public void setFrom(Long from)
    {
        this.from = from;
    }
    
    /**
     * Returns ID of the end destination of the flight.
     *
     * @return ID of the end destination of the flight
     */
    public Long getTo()
    {
        return to;
    }

    /**
     * Sets ID of the end destination of the flight.
     *
     * @param to ID of the end destination of the flight
     */
    public void setTo(Long to)
    {
        this.to = to;
    }

    /**
     * Returns URI of the flight entity.
     *
     * @return URI of the entity
     */
    public String getUri()
    {
        return uri;
    }

    /**
     * Sets URI of the flight entity.
     *
     * @param uri URI of the entity
     */
    public void setUri(String uri)
    {
        this.uri = uri;
    }

    /**
     * Implements the hashCode method for this class.
     *
     * @return The hash code
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((dateOfDeparture == null) ? 0 : dateOfDeparture.hashCode());
        result = prime * result + (new Float(distance)).hashCode();
        result = prime * result + seats;
        result = prime * result + (new Float(price)).hashCode();
        result = prime * result + ((from == null) ? 0 : from.hashCode());
        result = prime * result + ((to == null) ? 0 : to.hashCode());
        result = prime * result + ((uri == null) ? 0 : uri.hashCode());
        return result;
    }

    /**
     * Implements the equals method for this class.
     *
     * @return The equality boolean
     */
    @Override
    public boolean equals(Object obj)
    {
        return getClass() == obj.getClass() && this.getId().equals(((FlightEntity)obj).getId());
    }
}
