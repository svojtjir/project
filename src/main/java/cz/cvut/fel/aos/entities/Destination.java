/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.entities;

import cz.cvut.fel.aos.dbentities.DestinationEntity;
import java.io.Serializable;

/**
 * This class represents a flight destination entity.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
public class Destination implements Serializable
{
    private Long id;
    private String name;
    private float lat;
    private float lon;
    private String uri;

    /**
     * Returns ID of the destination entity.
     *
     * @return ID of the entity.
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Sets ID of the destination entity.
     *
     * @param id ID of the entity
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Returns name of the destination entity.
     *
     * @return Name of the entity.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets name of the destination entity.
     *
     * @param name Name of the entity
     */
    public void setName(String name)
    {
        this.name = name;
    }
    
    /**
     * Returns latitude of the destination entity.
     *
     * @return Latitude of the entity.
     */
    public float getLat()
    {
        return lat;
    }

    /**
     * Sets latitude of the destination entity.
     *
     * @param lat Latitude of the entity
     */
    public void setLat(float lat)
    {
        this.lat = lat;
    }
    
    /**
     * Returns longitude of the destination entity.
     *
     * @return Longitude of the entity.
     */
    public float getLon()
    {
        return lon;
    }

    /**
     * Sets longitude of the destination entity.
     *
     * @param lon Longitude of the entity
     */
    public void setLon(float lon)
    {
        this.lon = lon;
    }

    /**
     * Returns URI of the destination entity.
     *
     * @return URI of the entity
     */
    public String getUri()
    {
        return uri;
    }

    /**
     * Sets URI of the destination entity.
     *
     * @param uri URI of the entity
     */
    public void setUri(String uri)
    {
        this.uri = uri;
    }

    /**
     * Implements the hashCode method for this class.
     *
     * @return The hash code
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + (new Float(lat)).hashCode();
        result = prime * result + (new Float(lon)).hashCode();
        result = prime * result + ((uri == null) ? 0 : uri.hashCode());
        return result;
    }

    /**
     * Implements the equals method for this class.
     *
     * @return The equality boolean
     */
    @Override
    public boolean equals(Object obj)
    {
        return getClass() == obj.getClass() && this.getId().equals(((DestinationEntity)obj).getId());
    }
}
