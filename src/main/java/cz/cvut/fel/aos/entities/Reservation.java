/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.entities;

import cz.cvut.fel.aos.dbentities.ReservationEntity;
import java.io.Serializable;

/**
 * This class represents a flight reservation entity.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
public class Reservation implements Serializable
{
    private Long id;
    private Long flight;
    private String created;
    private String password;
    private int seats;
    private String state;
    private String uri;

    /**
     * Returns ID of the reservation entity.
     *
     * @return ID of the entity.
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Sets ID of the reservation entity.
     *
     * @param id ID of the entity
     */
    public void setId(Long id)
    {
        this.id = id;
    }
    
    /**
     * Returns ID of the flight of the reservation entity.
     *
     * @return ID of the flight of the entity.
     */
    public Long getFlight()
    {
        return flight;
    }

    /**
     * Sets ID of the flight of the reservation entity.
     *
     * @param flight ID of the flight of the entity
     */
    public void setFlight(Long flight)
    {
        this.flight = flight;
    }
    
    /**
     * Returns date of creation of the reservation entity as a string in format ISO8601.
     *
     * @return Date of creation of the entity
     */
    public String getCreated()
    {
        return created;
    }

    /**
     * Sets date of creation of the reservation entity as a string in format ISO8601.
     *
     * @param created Date of creation of the reservation entity
     */
    public void setCreated(String created)
    {
        this.created = created;
    }
    
    /**
     * Returns password of the reservation entity.
     *
     * @return Password of the entity
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * Sets password of the reservation entity.
     *
     * @param password Password of the entity
     */
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    /**
     * Returns number of reserved seats in the plane.
     *
     * @return Number of reserved seats
     */
    public int getSeats()
    {
        return seats;
    }

    /**
     * Sets number of reserved seats in the plane.
     *
     * @param seats Number of reserved seats in the plane
     */
    public void setSeats(int seats)
    {
        this.seats = seats;
    }
    
    /**
     * Returns state of the reservation entity.
     *
     * @return State of the entity
     */
    public String getState()
    {
        return state;
    }

    /**
     * Sets state of the reservation entity.
     *
     * @param state State of the reservation entity
     */
    public void setState(String state)
    {
        this.state = state;
    }
    
    /**
     * Returns URI of the reservation entity.
     *
     * @return URI of the entity
     */
    public String getUri()
    {
        return uri;
    }

    /**
     * Sets URI of the reservation entity.
     *
     * @param uri URI of the entity
     */
    public void setUri(String uri)
    {
        this.uri = uri;
    }

    /**
     * Implements the hashCode method for this class.
     *
     * @return The hash code
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((flight == null) ? 0 : flight.hashCode());
        result = prime * result + ((created == null) ? 0 : created.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + seats;
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        result = prime * result + ((uri == null) ? 0 : uri.hashCode());
        return result;
    }

    /**
     * Implements the equals method for this class.
     *
     * @return The equality boolean
     */
    @Override
    public boolean equals(Object obj)
    {
        return getClass() == obj.getClass() && this.getId().equals(((ReservationEntity)obj).getId());
    }
}
