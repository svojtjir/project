/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.entities.mapping;

import cz.cvut.fel.aos.entities.Flight;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class maps a set of flights into XML structure.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
@XmlRootElement(name="aos-flights")
public class FlightsMapping
{
    private List<Flight> flights;
    
    /**
     * Default constructor
     */
    public FlightsMapping(){}
    
    /**
     * Returns a list of assigned flights.
     *
     * @return The flights
     */
    public List<Flight> getFlights()
    {
        return this.flights;
    }
    
    /**
     * Assigns a list of flights to the mapping object.
     *
     * @param flights A list to assign
     */
    public void setFlights(List<Flight> flights)
    {
        this.flights = flights;
    }
}
