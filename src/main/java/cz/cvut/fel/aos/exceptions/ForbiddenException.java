/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.exceptions;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.*;

/**
 * This class represents an exception to be thrown and cause error 403 to be
 * returned to the requester.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
public class ForbiddenException extends WebApplicationException
{
    private String error;

    /**
     * Constructor
     *
     * @param error Description of the error
     */
    public ForbiddenException(String error)
    {
        super(Response.status(Status.FORBIDDEN).type(MediaType.TEXT_PLAIN).entity(error).build());
        this.error = error;
    }

    /**
     * Returns description of the error.
     *
     * @return The description
     */
    public String getError()
    {
        return this.error;
    }
}
