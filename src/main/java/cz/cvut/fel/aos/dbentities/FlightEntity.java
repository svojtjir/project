/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.dbentities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * This class maps flight entitities to database entries using JPA.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
@Entity
@Table(name = "Flight")
public class FlightEntity implements Serializable
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;
    
    @Column(name = "dateOfDeparture", nullable = false)
    private Long dateOfDeparture;
    
    @Column(name = "distance", nullable = false)
    private int distance;
    
    @Column(name = "seats", nullable = false)
    private int seats;
    
    @Column(name = "price", nullable = false)
    private int price;
    
    @Column(name = "fromDestination", nullable = false)
    private Long fromDestination;
    
    @Column(name = "toDestination", nullable = false)
    private Long toDestination;

    /**
     * Default constructor
     */
    public FlightEntity()
    {
        this.id = null;
        this.name = null;
        this.dateOfDeparture = 0L;
        this.distance = 0;
        this.seats = 0;
        this.price = 0;
        this.fromDestination = 0L;
        this.toDestination = 0L;
    }

    /**
     * Full constructor which initializes all object properties.
     *
     * @param id              A unique identifier of the flight entity
     * @param name            Name of the flight entity
     * @param dateOfDeparture Date of departure of the flight entity as a number of milliseconds since January 1, 1970
     * @param distance        Distance of the flight
     * @param seats           Number of seats in the plane
     * @param price           Price of a single ticket
     * @param from            ID of the start destination
     * @param to              ID of the end destination
     */
    public FlightEntity(Long id, String name, Long dateOfDeparture, int distance, int seats, int price, Long from, Long to)
    {
        this.id = id;
        this.name = name;
        this.dateOfDeparture = dateOfDeparture;
        this.distance = distance;
        this.seats = seats;
        this.price = price;
        this.fromDestination = from;
        this.toDestination = to;
    }

    /**
     * Returns ID of the flight entity.
     *
     * @return ID of the entity.
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Sets ID of the flight entity.
     *
     * @param id ID of the entity
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Returns name of the flight entity.
     *
     * @return Name of the entity.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets name of the flight entity.
     *
     * @param name Name of the entity
     */
    public void setName(String name)
    {
        this.name = name;
    }
    
    /**
     * Returns date of departure of the flight entity.
     *
     * @return Date of departure of the entity.
     */
    public Long getDateOfDeparture()
    {
        return dateOfDeparture;
    }

    /**
     * Sets date of departure of the flight entity.
     *
     * @param dateOfDeparture Date of departure of the entity as a number of milliseconds since January 1, 1970
     */
    public void setDateOfDeparture(Long dateOfDeparture)
    {
        this.dateOfDeparture = dateOfDeparture;
    }
    
    /**
     * Returns distance of the flight.
     *
     * @return Distance of the flight.
     */
    public int getDistance()
    {
        return distance;
    }

    /**
     * Sets distance of the flight.
     *
     * @param distance Distance of the flight
     */
    public void setDistance(int distance)
    {
        this.distance = distance;
    }
    
    /**
     * Returns number of seats in the plane.
     *
     * @return Number of seats in the plane
     */
    public int getSeats()
    {
        return seats;
    }

    /**
     * Sets number of seats in the plane.
     *
     * @param seats Number of seats in the plane
     */
    public void setSeats(int seats)
    {
        this.seats = seats;
    }
    
    /**
     * Returns price of a single ticket.
     *
     * @return Price of a single ticket
     */
    public int getPrice()
    {
        return price;
    }

    /**
     * Sets price of a single ticket.
     *
     * @param price Price of a single ticket
     */
    public void setPrice(int price)
    {
        this.price = price;
    }
    
    /**
     * Returns ID of the start destination of the flight.
     *
     * @return ID of the start destination of the flight
     */
    public Long getFrom()
    {
        return fromDestination;
    }

    /**
     * Sets ID of the start destination of the flight.
     *
     * @param from ID of the start destination of the flight
     */
    public void setFrom(Long from)
    {
        this.fromDestination = from;
    }
    
    /**
     * Returns ID of the end destination of the flight.
     *
     * @return ID of the end destination of the flight
     */
    public Long getTo()
    {
        return toDestination;
    }

    /**
     * Sets ID of the end destination of the flight.
     *
     * @param to ID of the end destination of the flight
     */
    public void setTo(Long to)
    {
        this.toDestination = to;
    }

    /**
     * Implements the hashCode method for this class.
     *
     * @return The hash code
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((dateOfDeparture == null) ? 0 : dateOfDeparture.hashCode());
        result = prime * result + (new Float(distance)).hashCode();
        result = prime * result + seats;
        result = prime * result + (new Float(price)).hashCode();
        result = prime * result + ((fromDestination == null) ? 0 : fromDestination.hashCode());
        result = prime * result + ((toDestination == null) ? 0 : toDestination.hashCode());
        return result;
    }

    /**
     * Implements the equals method for this class.
     *
     * @return The equality boolean
     */
    @Override
    public boolean equals(Object obj)
    {
        return getClass() == obj.getClass() && this.getId().equals(((FlightEntity)obj).getId());
    }
}
