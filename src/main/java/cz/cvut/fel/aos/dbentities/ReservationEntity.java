/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.dbentities;

import cz.cvut.fel.aos.utils.Utils;
import java.io.Serializable;
import javax.persistence.*;

/**
 * This class maps reservation entitities to database entries using JPA.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
@Entity
@Table(name = "Reservation")
public class ReservationEntity implements Serializable
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "flight", nullable = false)
    private Long flight;

    @Column(name = "created", nullable = false)
    private Long created;
    
    @Column(name = "password", nullable = false)
    private String password;
    
    @Column(name = "seats", nullable = false)
    private int seats;
    
    @Column(name = "resState", nullable = false)
    private String resState;

    /**
     * Default constructor
     */
    public ReservationEntity()
    {
        this.id = null;
        this.flight = null;
        this.created = null;
        this.password = null;
        this.seats = 0;
        this.resState = null;
    }

    /**
     * Full constructor which initializes all object properties.
     *
     * @param id       A unique identifier of the reservation entity
     * @param flight   ID of the flight to reserve
     * @param created  Date of the reservation creation as a number of milliseconds since January 1, 1970
     * @param password Password of the reservation entity
     * @param seats    Number of seats to be reserved
     * @param state    State of the reservation entity
     */
    public ReservationEntity(Long id, Long flight, Long created, String password, int seats, String state)
    {
        this.id = id;
        this.flight = flight;
        this.created = created;
        this.password = password;
        this.seats = seats;
        this.resState = state;
    }

    /**
     * Returns ID of the reservation entity.
     *
     * @return ID of the entity.
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Sets ID of the reservation entity.
     *
     * @param id ID of the entity
     */
    public void setId(Long id)
    {
        this.id = id;
    }
    
    /**
     * Returns ID of the flight of the reservation entity.
     *
     * @return ID of the flight of the entity.
     */
    public Long getFlight()
    {
        return flight;
    }

    /**
     * Sets ID of the flight of the reservation entity.
     *
     * @param flight ID of the flight of the entity
     */
    public void setFlight(Long flight)
    {
        this.flight = flight;
    }
    
    /**
     * Returns date of creation of the reservation entity as a number of milliseconds since January 1, 1970.
     *
     * @return Date of creation of the entity
     */
    public long getCreated()
    {
        return created;
    }

    /**
     * Sets date of creation of the reservation entity as a number of milliseconds since January 1, 1970.
     *
     * @param created Date of creation of the reservation entity
     */
    public void setCreated(Long created)
    {
        this.created = created;
    }
    
    /**
     * Sets date of creation of the reservation entity as a string in format ISO8601.
     *
     * @param created Date of creation of the reservation entity
     */
    public void setCreated(String created)
    {
        this.created = Utils.dateStringToLong(created);
    }
    
    /**
     * Returns password of the reservation entity.
     *
     * @return Password of the entity
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * Sets password of the reservation entity.
     *
     * @param password Password of the entity
     */
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    /**
     * Returns number of reserved seats in the plane.
     *
     * @return Number of reserved seats
     */
    public int getSeats()
    {
        return seats;
    }

    /**
     * Sets number of reserved seats in the plane.
     *
     * @param seats Number of reserved seats in the plane
     */
    public void setSeats(int seats)
    {
        this.seats = seats;
    }
    
    /**
     * Returns state of the reservation entity.
     *
     * @return State of the entity
     */
    public String getState()
    {
        return resState;
    }

    /**
     * Sets state of the reservation entity.
     *
     * @param state State of the reservation entity
     */
    public void setState(String state)
    {
        this.resState = state;
    }

    /**
     * Implements the hashCode method for this class.
     *
     * @return The hash code
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((flight == null) ? 0 : flight.hashCode());
        result = prime * result + ((created == null) ? 0 : created.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + seats;
        result = prime * result + ((resState == null) ? 0 : resState.hashCode());
        return result;
    }

    /**
     * Implements the equals method for this class.
     *
     * @return The equality boolean
     */
    @Override
    public boolean equals(Object obj)
    {
        return getClass() == obj.getClass() && this.getId().equals(((ReservationEntity)obj).getId());
    }
}
