/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.dbentities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * This class maps destination entitities to database entries using JPA.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
@Entity
@Table(name = "Destination")
public class DestinationEntity implements Serializable
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;
    
    @Column(name = "lat", nullable = false)
    private float lat;
    
    @Column(name = "lon", nullable = false)
    private float lon;

    /**
     * Default constructor
     */
    public DestinationEntity()
    {
        this.id = null;
        this.name = null;
        this.lat = 0;
        this.lon = 0;
    }

    /**
     * Full constructor which initializes all object properties.
     *
     * @param id   A unique identifier of the destination entity
     * @param name Name of the destination entity
     * @param lat  Latitude of the destination entity
     * @param lon  Longitude of the destination entity
     */
    public DestinationEntity(Long id, String name, float lat, float lon)
    {
        this.id = id;
        this.name = name;
        this.lat = lat;
        this.lon = lon;
    }

    /**
     * Returns ID of the destination entity.
     *
     * @return ID of the entity.
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Sets ID of the destination entity.
     *
     * @param id ID of the entity
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Returns name of the destination entity.
     *
     * @return Name of the entity.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets name of the destination entity.
     *
     * @param name Name of the entity
     */
    public void setName(String name)
    {
        this.name = name;
    }
    
    /**
     * Returns latitude of the destination entity.
     *
     * @return Latitude of the entity.
     */
    public float getLat()
    {
        return lat;
    }

    /**
     * Sets latitude of the destination entity.
     *
     * @param lat Latitude of the entity
     */
    public void setLat(float lat)
    {
        this.lat = lat;
    }
    
    /**
     * Returns longitude of the destination entity.
     *
     * @return Longitude of the entity.
     */
    public float getLon()
    {
        return lon;
    }

    /**
     * Sets longitude of the destination entity.
     *
     * @param lon Longitude of the entity
     */
    public void setLon(float lon)
    {
        this.lon = lon;
    }

    /**
     * Implements the hashCode method for this class.
     *
     * @return The hash code
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + (new Float(lat)).hashCode();
        result = prime * result + (new Float(lon)).hashCode();
        return result;
    }

    /**
     * Implements the equals method for this class.
     *
     * @return The equality boolean
     */
    @Override
    public boolean equals(Object obj)
    {
        return getClass() == obj.getClass() && this.getId().equals(((DestinationEntity)obj).getId());
    }
}
