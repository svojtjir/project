/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import cz.cvut.fel.aos.dbentities.DestinationEntity;
import cz.cvut.fel.aos.dbentities.FlightEntity;
import cz.cvut.fel.aos.exceptions.BadRequestException;
import cz.cvut.fel.aos.exceptions.NotFoundException;
import cz.cvut.fel.aos.utils.Utils;
import java.util.List;
import javax.persistence.Query;

/**
 * This class handles manipulation with database destination entities.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
public class DestinationDao
{
    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("persist-unit");

    /**
     * Returns a single destination.
     *
     * @param id ID of the destination
     * @return The requested destination
     */
    public DestinationEntity get(Long id)
    {
        EntityManager em = emf.createEntityManager();
        DestinationEntity entity = null;
        try
        {
            entity = em.find(DestinationEntity.class, id);
            if(entity == null)
                throw new NotFoundException("No such destination");
        }
        finally
        {
            em.close();
        }
        return entity;
    }
    
    /**
     * Returns multiple destinations.
     *
     * @param orderBy  Property to be used as an ordering criterion; can be null
     * @param orderDir Ordering direction; can be null or either ASC or DESC
     * @param base     Maximal number of entities to return; use 0 for no limit
     * @param offset   Offset of the entities to return
     * @return The requested destinations
     */
    public List<DestinationEntity> get(String orderBy, String orderDir, int base, int offset)
    {
        String query = "SELECT d FROM DestinationEntity d";

        if(orderBy != null && orderDir != null)
            query += " ORDER BY d." + orderBy + " " + orderDir;
        
        EntityManager em = emf.createEntityManager();
        List<DestinationEntity> entities = null;
        try
        {
            Query q = em.createQuery(query);
            if(base != 0)
                q.setMaxResults(base).setFirstResult(offset);
            entities = q.getResultList();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            em.close();
        }

        return entities;
    }
    
    /**
     * Returns the count of all destinations stored in the database.
     *
     * @return The count
     */
    public Long getCount()
    {
        Long count = 0L;
        EntityManager em = emf.createEntityManager();
        try
        {
            Query q = em.createQuery("SELECT COUNT(d) FROM DestinationEntity d");
            count = (Long)q.getSingleResult();
        }
        finally
        {
            em.close();
        }
        return count;
    }

    /**
     * Removes a destination from the database.
     *
     * @param id ID of the destination to remove
     */
    public void delete(Long id)
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try
        {
            transaction.begin();
            DestinationEntity entity = em.find(DestinationEntity.class, id);
            if(entity == null)
                throw new NotFoundException("No such destination");
            em.remove(entity);
            transaction.commit();
        }
        finally
        {
            em.close();
        }
    }

    /**
     * Inserts a new destination into the database.
     *
     * @param destination The destination to insert
     */
    public void add(DestinationEntity destination)
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try
        {
            transaction.begin();
            em.persist(destination);
            transaction.commit();
        }
        finally
        {
            em.close();
        }
    }

    /**
     * Updates a destination.
     *
     * @param destination The destination to update
     */
    public void update(DestinationEntity destination)
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try
        {
            transaction.begin();
            em.merge(destination);
            transaction.commit();
        }
        finally
        {
            em.close();
        }
    }
}
