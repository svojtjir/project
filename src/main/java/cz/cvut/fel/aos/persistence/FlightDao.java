/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import cz.cvut.fel.aos.dbentities.FlightEntity;
import cz.cvut.fel.aos.exceptions.BadRequestException;
import cz.cvut.fel.aos.exceptions.NotFoundException;
import cz.cvut.fel.aos.utils.Utils;
import java.util.List;
import javax.persistence.Query;

/**
 * This class handles manipulation with database flight entities.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
public class FlightDao
{
    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("persist-unit");

    /**
     * Returns a single flight.
     *
     * @param id ID of the flight
     * @return The requested flight
     */
    public FlightEntity get(Long id)
    {
        EntityManager em = emf.createEntityManager();
        FlightEntity entity = null;
        try
        {
            entity = em.find(FlightEntity.class, id);
            if(entity == null)
                throw new NotFoundException("No such flight");
        }
        finally
        {
            em.close();
        }
        return entity;
    }

    /**
     * Returns multiple flights.
     *
     * @param departureFrom Lowest daparture date of flights to be included in the result
     * @param departureTo Highest daparture date of flights to be included in the result
     * @param orderBy  Property to be used as an ordering criterion; can be null
     * @param orderDir Ordering direction; can be null or either ASC or DESC
     * @param base     Maximal number of entities to return; use 0 for no limit
     * @param offset   Offset of the entities to return
     * @return The requested flights
     */
    public List<FlightEntity> get(String departureFrom, String departureTo, String orderBy, String orderDir, int base, int offset)
    {
        String query = "SELECT f FROM FlightEntity f";
        String whereCl = "";
        String orderCl = "";

        if(departureFrom != null && departureTo != null)
        {
            Long fromLong = Utils.dateStringToLong(departureFrom);
            Long toLong = Utils.dateStringToLong(departureTo);
            whereCl += " f.dateOfDeparture >= "+fromLong+" AND f.dateOfDeparture <= "+toLong;
        }

        if(orderBy != null && orderDir != null)
        {
            if(orderBy.equals("from"))
            {
                query += ", DestinationEntity d";
                whereCl += ((whereCl.equals("")) ? "" : " AND") + " f.fromDestination = d.id";
                orderCl = " ORDER BY d.name " + orderDir;
            }
            if(orderBy.equals("to"))
            {
                query += ", DestinationEntity d";
                whereCl += ((whereCl.equals("")) ? "" : " AND") + " f.toDestination = d.id";
                orderCl = " ORDER BY d.name " + orderDir;
            }
            else
            {
                if(orderBy.equals("from"))
                    orderBy = "fromDestination";
                if(orderBy.equals("to"))
                    orderBy = "toDestination";
                orderCl = " ORDER BY f." + orderBy + " " + orderDir;
            }
        }
        
        if(!whereCl.equals(""))
            query += " WHERE"+whereCl;
        query += orderCl;

        EntityManager em = emf.createEntityManager();
        List<FlightEntity> entities = null;
        try
        {
            Query q = em.createQuery(query);
            if(base != 0)
                q.setMaxResults(base).setFirstResult(offset);
            entities = q.getResultList();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            em.close();
        }
        return entities;
    }
    
    /**
     * Returns the count of all flights stored in the database.
     *
     * @return The count
     */
    public Long getCount(String departureFrom, String departureTo)
    {
        String query = "SELECT COUNT(f) FROM FlightEntity f";
        if(departureFrom != null && departureTo != null)
        {
            Long fromLong = Utils.dateStringToLong(departureFrom);
            Long toLong = Utils.dateStringToLong(departureTo);
            query += " WHERE f.dateOfDeparture >= "+fromLong+" AND f.dateOfDeparture <= "+toLong;
        }
        Long count = 0L;
        EntityManager em = emf.createEntityManager();
        try
        {
            Query q = em.createQuery(query);
            count = (Long)q.getSingleResult();
        }
        finally
        {
            em.close();
        }
        return count;
    }

    /**
     * Removes a flight from the database.
     *
     * @param id ID of the flight to remove
     */
    public void delete(Long id)
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try
        {
            transaction.begin();
            FlightEntity entity = em.find(FlightEntity.class, id);
            if(entity == null)
                throw new NotFoundException("No such flight");
            em.remove(entity);
            transaction.commit();
        }
        finally
        {
            em.close();
        }
    }

    /**
     * Inserts a new flight into the database.
     *
     * @param flight The flight to insert
     */
    public void add(FlightEntity flight)
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try
        {
            transaction.begin();
            em.persist(flight);
            transaction.commit();
        }
        finally
        {
            em.close();
        }
    }
    
    /**
     * Updates a flight.
     *
     * @param flight The flight to update
     */
    public void update(FlightEntity flight)
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try
        {
            transaction.begin();
            em.merge(flight);
            transaction.commit();
        }
        finally
        {
            em.close();
        }
    }
}
