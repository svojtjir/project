/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.persistence;

import cz.cvut.fel.aos.dbentities.DestinationEntity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import cz.cvut.fel.aos.dbentities.ReservationEntity;
import cz.cvut.fel.aos.exceptions.BadRequestException;
import cz.cvut.fel.aos.exceptions.NotFoundException;
import cz.cvut.fel.aos.utils.Utils;
import java.util.List;
import javax.persistence.Query;

/**
 * This class handles manipulation with database reservation entities.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
public class ReservationDao
{
    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("persist-unit");

    /**
     * Returns a single reservation.
     *
     * @param id ID of the reservation
     * @return The requested reservation
     */
    public ReservationEntity get(Long id)
    {
        EntityManager em = emf.createEntityManager();
        ReservationEntity entity = null;
        try
        {
            entity = em.find(ReservationEntity.class, id);
            if(entity == null)
                throw new NotFoundException("No such reservation");
        }
        finally
        {
            em.close();
        }
        return entity;
    }
    
    /**
     * Returns multiple reservations.
     *
     * @param orderBy  Property to be used as an ordering criterion; can be null
     * @param orderDir Ordering direction; can be null or either ASC or DESC
     * @param base     Maximal number of entities to return; use 0 for no limit
     * @param offset   Offset of the entities to return
     * @return The requested reservations
     */
    public List<ReservationEntity> get(String orderBy, String orderDir, int base, int offset)
    {
        String query = "SELECT r FROM ReservationEntity r";

        if(orderBy != null && orderDir != null)
            query += " ORDER BY r." + orderBy + " " + orderDir;
        
        EntityManager em = emf.createEntityManager();
        List<ReservationEntity> entities = null;
        try
        {
            Query q = em.createQuery(query);
            if(base != 0)
                q.setMaxResults(base).setFirstResult(offset);
            entities = q.getResultList();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            em.close();
        }

        return entities;
    }
    
    /**
     * Returns the count of all reservations stored in the database.
     *
     * @return The count
     */
    public Long getCount()
    {
        Long count = 0L;
        EntityManager em = emf.createEntityManager();
        try
        {
            Query q = em.createQuery("SELECT COUNT(r) FROM ReservationEntity r");
            count = (Long)q.getSingleResult();
        }
        finally
        {
            em.close();
        }
        return count;
    }

    /**
     * Removes a reservation from the database.
     *
     * @param id ID of the reservation to remove
     */
    public void delete(Long id)
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try
        {
            transaction.begin();
            ReservationEntity entity = em.find(ReservationEntity.class, id);
            if(entity == null)
                throw new NotFoundException("No such reservation");
            em.remove(entity);
            transaction.commit();
        }
        finally
        {
            em.close();
        }
    }

    /**
     * Inserts a new reservation into the database.
     *
     * @param reservation The reservation to insert
     */
    public void add(ReservationEntity reservation)
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try
        {
            transaction.begin();
            em.persist(reservation);
            transaction.commit();
        }
        finally
        {
            em.close();
        }
    }
    
    /**
     * Updates a reservation.
     *
     * @param reservation The reservation to update
     */
    public void update(ReservationEntity reservation)
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try
        {
            transaction.begin();
            em.merge(reservation);
            transaction.commit();
        }
        finally
        {
            em.close();
        }
    }
}
