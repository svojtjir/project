/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.utils;

import cz.cvut.fel.aos.exceptions.BadRequestException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class provides static methods for date conversions.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
public class Utils
{
    /**
     * Converts ISO8601 to number of milliseconds since January 1, 1970.
     *
     * @param date The date string to convert
     */
    public static Long dateStringToLong(String date)
    {
        try
        {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            date = date.substring(0, date.length()-3) + date.substring(date.length()-2);
            return df.parse(date).getTime();
        }
        catch(Exception e)
        {
            throw new BadRequestException("Invalid date format");
        }
    }
    
    /**
     * Converts number of milliseconds since January 1, 1970 to ISO8601.
     *
     * @param milliseconds The number of milliseconds
     */
    public static String dateLongToString(Long milliseconds)
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        String d = df.format(new Date(milliseconds));
        return d.substring(0, d.length()-2) + ":" + d.substring(d.length()-2);
    }
}
