/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.RemoteEndpoint;
import javax.websocket.RemoteEndpoint.Basic;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 * This class is responsible for counting connected Airline clients.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
@ServerEndpoint("/connected")
public class ConnectedEndpoint
{
    private static int connected = 0;
    private static List<RemoteEndpoint.Basic> clients;

    /**
     * Constructor
     */
    public ConnectedEndpoint()
    {
        if(ConnectedEndpoint.clients == null)
          ConnectedEndpoint.clients = new ArrayList<RemoteEndpoint.Basic>();
    }
    
    /**
     * Increments the counter when a new client connects to the service and
     * sends the new count to all connected clients.
     *
     * @param session Session of the connected client
     */
    @OnOpen
    public void onOpen(Session session) throws IOException
    {
        RemoteEndpoint.Basic client = session.getBasicRemote();
        ConnectedEndpoint.clients.add(client);
        ConnectedEndpoint.connected++;
        
        for(int i = 0; i < ConnectedEndpoint.connected; i++)
            ConnectedEndpoint.clients.get(i).sendText(String.valueOf(ConnectedEndpoint.connected));
    }

    /**
     * Prints an error when such occurs during the communication.
     */
    @OnError
    public void onError(Throwable t)
    {
        t.printStackTrace();
    }
}
