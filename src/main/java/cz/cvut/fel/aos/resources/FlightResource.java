/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import cz.cvut.fel.aos.entities.Flight;
import cz.cvut.fel.aos.entities.mapping.FlightsMapping;
import cz.cvut.fel.aos.services.FlightService;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.core.Response;

/**
 * This class implements a REST resource providing flights.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
@Path(value = "/flight/")
public class FlightResource
{
    private final FlightService flightService;

    /**
     * Constructor
     */
    public FlightResource()
    {
        this.flightService = new FlightService();
    }
    
    /**
     * Returns multiple flights.
     *
     * @param filter Filter in "dateOfDepartureFrom=xxx, dateOfDepartureTo=xxx" format; can be null
     * @param order  Ordering criterion in "property: ASC|DESC" format; can be null
     * @param base   Maximal number of entities to return; use 0 for no limit
     * @param offset Offset of the entities to return
     * @param currency The currency to apply on returning flight prices
     * @return A response containing the requested flights
     */
    @GET
    @Path(value = "/")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Response getFlights(@HeaderParam("X-Filter") String filter, @HeaderParam("X-Order") String order, @HeaderParam("X-Base") int base, @HeaderParam("X-Offset") int offset, @HeaderParam("X-Currency") String currency)
    {
        String departureFrom = null;
        String departureTo = null;
        String orderBy = null;
        String orderDir = null;
        
        if(filter != null)
        {
            String[] vars = filter.split(",");
            for(int i = 0; i < 2; i++)
            {
                String[] pair = vars[i].split("=");
                if(pair[0].equals("dateOfDepartureFrom"))
                    departureFrom = pair[1];
                else if(pair[0].equals("dateOfDepartureTo"))
                    departureTo = pair[1];
            }
        }
        
        if(order != null)
        {
            String[] pair = order.split(":");
            orderBy = pair[0].trim();
            orderDir = pair[1].trim().toUpperCase();
        }

        List<Flight> flights = flightService.get(departureFrom, departureTo, orderBy, orderDir, base, offset, currency);
        
        FlightsMapping flightsMapping = new FlightsMapping();
        flightsMapping.setFlights(flights);
        
        Long records = flightService.getCount(departureFrom, departureTo);

        return Response.ok(flightsMapping).header("X-Count-records", records).build();
    }

    /**
     * Returns a single flight.
     *
     * @param id ID of the flight to return
     * @return A response containing the requested flight
     */
    @GET
    @Path(value = "/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Flight getFlight(@PathParam("id") Long id)
    {
        return flightService.get(id);
    }
    
    /**
     * Creates a new flight.
     *
     * @param flight The flight to create
     * @return A response containing the created flight
     */
    @POST
    @Path(value = "/")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Flight addFlight(Flight flight)
    {
        return flightService.add(flight);
    }
    
    /**
     * Updates a flight.
     *
     * @param id ID of the flight to update
     * @param flight The flight to update
     * @return A response containing the created flight
     */
    @PUT
    @Path(value = "/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Flight updateFlight(@PathParam("id") Long id, Flight flight)
    {
        return flightService.update(id, flight);
    }

    /**
     * Removes a flight.
     *
     * @param id ID of the flight to remove
     * @return A response containing the removed flight
     */
    @DELETE
    @Path(value = "/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Flight deleteFlight(@PathParam("id") Long id)
    {
        return flightService.delete(id);
    }
}
