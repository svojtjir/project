/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.resources;

import cz.cvut.fel.aos.entities.Payment;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import cz.cvut.fel.aos.entities.Reservation;
import cz.cvut.fel.aos.entities.mapping.ReservationsMapping;
import cz.cvut.fel.aos.services.PrintService;
import cz.cvut.fel.aos.services.ReservationService;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.core.Response;

/**
 * This class implements a REST resource providing flight reservations.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
@Path(value = "/reservation/")
public class ReservationResource
{
    private final ReservationService reservationService;

    /**
     * Constructor
     */
    public ReservationResource()
    {
        this.reservationService = new ReservationService();
    }

    /**
     * Returns a single reservation.
     *
     * @param id ID of the reservation to return
     * @param password Password to access the reservation
     * @return A response containing the requested reservation
     */
    @GET
    @Path(value = "/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Reservation getReservation(@PathParam("id") Long id, @HeaderParam("X-Password") String password)
    {
        return reservationService.get(id, password);
    }
    
    /**
     * Returns multiple flights.
     *
     * @param order  Ordering criterion in "property: ASC|DESC" format; can be null
     * @param base   Maximal number of entities to return; use 0 for no limit
     * @param offset Offset of the entities to return
     * @return A response containing the requested flights
     */
    @GET
    @Path(value = "/")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Response getReservations(@HeaderParam("X-Order") String order, @HeaderParam("X-Base") int base, @HeaderParam("X-Offset") int offset)
    {
        String orderBy = null;
        String orderDir = null;
        if(order != null)
        {
            String[] pair = order.split(":");
            orderBy = pair[0].trim();
            orderDir = pair[1].trim().toUpperCase();
        }

        List<Reservation> reservations = reservationService.get(orderBy, orderDir, base, offset);
        
        ReservationsMapping reservationsMapping = new ReservationsMapping();
        reservationsMapping.setReservations(reservations);
        
        Long records = reservationService.getCount();
        return Response.ok(reservationsMapping).header("X-Count-records", records).build();
    }
    
    /**
     * Creates a new reservation.
     *
     * @param reservation The reservation to create
     * @return A response containing the created reservation
     */
    @POST
    @Path(value = "/")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Reservation addReservation(Reservation reservation)
    {
        return reservationService.add(reservation);
    }
    
    /**
     * Updates a reservation.
     *
     * @param id ID of the reservation to update
     * @param reservation The reservation to update
     * @param password Password to access the reservation
     * @return A response containing the created reservation
     */
    @PUT
    @Path(value = "/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Reservation updateReservation(@PathParam("id") Long id, Reservation reservation, @HeaderParam("X-Password") String password)
    {
        return reservationService.update(id, reservation, password);
    }

    /**
     * Removes a reservation.
     *
     * @param id ID of the reservation to remove
     * @return A response containing the removed reservation
     */
    @DELETE
    @Path(value = "/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Reservation deleteReservation(@PathParam("id") Long id)
    {
        return reservationService.delete(id);
    }
    
    /**
     * Performs a payment of a reservation.
     *
     * @param payment The payment to perform
     * @param id ID of the reservation to pay
     * @return A response containing the performed payment
     */
    @POST
    @Path(value = "/{id}/payment")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Payment addReservation(Payment payment, @PathParam("id") Long id)
    {
        return reservationService.addPayment(id, payment);
    }

    /**
     * Sends an e-ticket of paid flight reservation to the client's email address.
     *
     * @param id ID of the reservation
     * @param password Password to access the reservation
     * @param email An email address to use
     * @return An empty response
     */
    @GET
    @Path(value = "/{id}/print")
    public Response print(@PathParam("id") Long id, @HeaderParam("X-Password") String password, @HeaderParam("X-Email") String email)
    {
        Reservation res = reservationService.get(id, password);

        PrintService port = (new PrintService()).getPrintServicePort();
        
        port.sendEmail(res, email);
        return Response.ok().build();
    }
}
