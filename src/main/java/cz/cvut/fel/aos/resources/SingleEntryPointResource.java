/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.resources;

import cz.cvut.fel.aos.entities.Destination;
import cz.cvut.fel.aos.entities.Flight;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import cz.cvut.fel.aos.entities.Reservation;
import cz.cvut.fel.aos.services.DestinationService;
import cz.cvut.fel.aos.services.FlightService;
import cz.cvut.fel.aos.services.ReservationService;
import java.util.ArrayList;
import java.util.List;

/**
 * This class implements a REST resource providing destinations, flights and reservations.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
@Path("/all")
public class SingleEntryPointResource
{
    private final DestinationService destinationService = new DestinationService();
    private final FlightService flightService = new FlightService();
    private final ReservationService reservationService = new ReservationService();
    
    /**
     * Returns all destinations, flights and reservations.
     *
     * @return A response containing the objects
     */
    @GET
    @Path(value = "/")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Object> getAll()
    {
        List<Object> resources = new ArrayList<Object>();
        
        List<Destination> destinations = destinationService.get(null, null, 0, 0);
        for(Destination dest : destinations)
            resources.add(dest);
        
        List<Flight> flights = flightService.get(null, null, null, null, 0, 0, null);
        for(Flight flight : flights)
            resources.add(flight);
        
        List<Reservation> reservations = reservationService.get(null, null, 0, 0);
        for(Reservation res : reservations)
            resources.add(res);
        
        return resources;
    }
}
