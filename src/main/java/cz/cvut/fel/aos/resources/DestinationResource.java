/*
 * This class definition is a part of AOS Airline project. The project has been
 * developed as a semestral work of "Service Oriented Architectures" course.
 * It has never been considered to be used for any other purposes and it should
 * remain private.
 */

package cz.cvut.fel.aos.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import cz.cvut.fel.aos.entities.Destination;
import cz.cvut.fel.aos.entities.mapping.DestinationsMapping;
import cz.cvut.fel.aos.services.DestinationService;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.core.Response;

/**
 * This class implements a REST resource providing flight destinations.
 *
 * @author Jakub Machacek, Jiri Svojtka
 */
@Path(value = "/destination/")
public class DestinationResource
{
    private final DestinationService destinationService;

    /**
     * Constructor
     */
    public DestinationResource()
    {
        this.destinationService = new DestinationService();
    }
    
    /**
     * Returns multiple destinations.
     *
     * @param order  Ordering criterion in "property: ASC|DESC" format; can be null
     * @param base   Maximal number of entities to return; use 0 for no limit
     * @param offset Offset of the entities to return
     * @return A response containing the requested destinations
     */
    @GET
    @Path(value = "/")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Response getDestinations(@HeaderParam("X-Order") String order, @HeaderParam("X-Base") int base, @HeaderParam("X-Offset") int offset)
    {
        String orderBy = null;
        String orderDir = null;
        if(order != null)
        {
            String[] pair = order.split(":");
            orderBy = pair[0].trim();
            orderDir = pair[1].trim().toUpperCase();
        }

        List<Destination> destinations = destinationService.get(orderBy, orderDir, base, offset);
        
        DestinationsMapping destinationsMapping = new DestinationsMapping();
        destinationsMapping.setDestinations(destinations);
        
        Long records = destinationService.getCount();
        return Response.ok(destinationsMapping).header("X-Count-records", records).build();
    }

    /**
     * Returns a single destination.
     *
     * @param id ID of the destination to return
     * @return A response containing the requested destination
     */
    @GET
    @Path(value = "/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Destination getDestination(@PathParam("id") Long id)
    {
        return destinationService.get(id);
    }
    
    /**
     * Creates a new destination.
     *
     * @param destination The destination to create
     * @return A response containing the created destination
     */
    @POST
    @Path(value = "/")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Destination addDestination(Destination destination)
    {
        return destinationService.add(destination);
    }

    /**
     * Updates a destination.
     *
     * @param id ID of the destination to update
     * @param destination The destination to update
     * @return A response containing the created destination
     */
    @PUT
    @Path(value = "/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Destination updateDestination(@PathParam("id") Long id, Destination destination)
    {
        return destinationService.update(id, destination);
    }

    /**
     * Removes a destination.
     *
     * @param id ID of the destination to remove
     * @return A response containing the removed destination
     */
    @DELETE
    @Path(value = "/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @RolesAllowed({"manager", "admin"})
    public Destination deleteDestination(@PathParam("id") Long id)
    {
        return destinationService.delete(id);
    }
}
